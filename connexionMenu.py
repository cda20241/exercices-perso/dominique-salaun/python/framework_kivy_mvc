from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.textinput import TextInput

from gameMenu import GameMenu
from user import User

class ConnexionMenu(BoxLayout):
    """
    ConnexionMenu class represents the menu for user connection and game initiation.

    Attributes:
        label_first_name (Label): Label for the user's first name.
        input_first_name (TextInput): Input field for entering the user's first name.
        label_last_name (Label): Label for the user's last name.
        input_last_name (TextInput): Input field for entering the user's last name.
        submit_button (Button): Button for submitting the user information and starting the game.

    Methods:
        __init__: Initializes the ConnexionMenu instance.
        on_submit: Event handler for the submit button, creates a User instance and opens the GameMenu in a Popup.

    """
    def __init__(self, **kwargs):
        """
        Initialize the ConnexionMenu.

        Parameters:
            **kwargs: Additional keyword arguments for the BoxLayout.

        """
        super(ConnexionMenu, self).__init__(**kwargs)
        self.orientation = 'vertical'
        self.spacing = 10
        self.padding = 10

        self.label_first_name = Label(text='Prénom:')
        self.input_first_name = TextInput(multiline=False)
        self.label_last_name = Label(text='Nom:')
        self.input_last_name = TextInput(multiline=False)
        self.submit_button = Button(text='Jouer', on_press=self.on_submit)

        self.add_widget(self.label_first_name)
        self.add_widget(self.input_first_name)
        self.add_widget(self.label_last_name)
        self.add_widget(self.input_last_name)
        self.add_widget(self.submit_button)

    def on_submit(self, instance):
        """
        Event handler for the submit button.
        Creates a User instance using entered information and opens the GameMenu in a Popup.

        Parameters:
            instance: The button instance triggering the event.

        """
        user = User(self.input_first_name.text, self.input_last_name.text)
        game_menu = GameMenu(user)
        popup = Popup(title='Menu de Jeu', content=game_menu, size_hint=(None, None), size=(1000, 1000))
        popup.open()
