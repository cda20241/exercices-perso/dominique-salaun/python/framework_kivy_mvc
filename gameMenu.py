from kivy.core.window import Window
from kivy.core.window import Keyboard
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import Image

from character import Character


class GameMenu(BoxLayout):
    """
    GameMenu class represents the main game interface.

    Parameters:
        user (str): The user associated with the game.

    Attributes:
        labyrinth (Image): The background image displaying the labyrinth.
        character (Character): The player's character.
        button_up, button_down, button_left, button_right (Button): Buttons for directional movement.
        user (str): The user associated with the game.

    Methods:
        __init__: Initializes the GameMenu instance.
        on_key_up: Handles key release events, triggering upward movement.
        on_key_down: Handles key press events, triggering movement in different directions.
        move_up, move_down, move_left, move_right: Methods to update character position based on button presses.

    """
    def __init__(self, user, **kwargs):
        """
        Initialize the GameMenu.

        Parameters:
            user (str): The user associated with the game.
            **kwargs: Additional keyword arguments for the BoxLayout.

        """
        super(GameMenu, self).__init__(**kwargs)
        self.orientation = 'vertical'
        self.spacing = 10
        self.padding = 10

        # Retrieve the original size of the labyrinth image
        original_size = Image(source='labyrinthe.png').texture.size

        # Background with the labyrinth
        self.labyrinth = Image(source='labyrinthe.png', size=original_size, allow_stretch=False, keep_ratio=True)
        self.add_widget(self.labyrinth)

        # Player character
        self.character = Character()
        self.add_widget(self.character)

        # Layout for directional buttons
        button_layout = GridLayout(cols=2, spacing=10, size_hint_y=None, height=50)

        self.button_up = Button(text='Up', on_press=self.move_up)
        self.button_down = Button(text='Down', on_press=self.move_down)
        self.button_left = Button(text='Left', on_press=self.move_left)
        self.button_right = Button(text='Right', on_press=self.move_right)

        button_layout.add_widget(self.button_up)
        button_layout.add_widget(self.button_down)
        button_layout.add_widget(self.button_left)
        button_layout.add_widget(self.button_right)

        self.add_widget(button_layout)

        self.user = user

        # Bind arrow keys to movement methods
        Window.bind(on_key_up=self.on_key_up)
        Window.bind(on_key_down=self.on_key_down)

    def on_key_up(self, keyboard, keycode, text):
        """
        Handle key release events.

        Parameters:
            keyboard: The keyboard instance.
            keycode: The code associated with the pressed key.
            text: The text representation of the pressed key.

        """
        if isinstance(keycode, tuple):
            keycode = keycode[1]

        if keycode == 'up':
            self.move_up(None)

    def on_key_down(self, keyboard, keycode, text, modifiers, _):
        """
        Handle key press events.

        Parameters:
            keyboard: The keyboard instance.
            keycode: The code associated with the pressed key.
            text: The text representation of the pressed key.
            modifiers: The modifiers pressed along with the key.
            _: Placeholder for an unused argument.

        """

        keyboardInstance = Keyboard()

        if keycode == keyboardInstance.string_to_keycode('down'):
            self.move_down(None)
        elif keycode == keyboardInstance.string_to_keycode('left'):
            self.move_left(None)
        elif keycode == keyboardInstance.string_to_keycode('right'):
            self.move_right(None)
        elif keycode == keyboardInstance.string_to_keycode('up'):
            self.move_up(None)
    def move_up(self, instance):
        """
        Move the character upward.

        Parameters:
            instance: The button instance triggering the movement.

        """
        self.character.flip()
        self.character.direction = 'up'
        self.character.pos[1] += 10

    def move_down(self, instance):
        """
        Move the character downward.

        Parameters:
            instance: The button instance triggering the movement.

        """
        self.character.flip()
        self.character.direction = 'down'
        self.character.pos[1] -= 10

    def move_left(self, instance):
        """
        Move the character to the left.

        Parameters:
            instance: The button instance triggering the movement.

        """
        self.character.flip()
        self.character.direction = 'left'
        self.character.pos[0] -= 10

    def move_right(self, instance):
        """
        Move the character to the right.

        Parameters:
            instance: The button instance triggering the movement.

        """
        self.character.flip()
        self.character.direction = 'right'
        self.character.pos[0] += 10
