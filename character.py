from kivy.clock import Clock
from kivy.uix.image import Image

class Character(Image):
    """
    Character class represents the player's character in the game.

    Attributes:
        source_left (str): Image source for the character facing left.
        source_right (str): Image source for the character facing right.
        source_up (str): Image source for the character facing up.
        source_down (str): Image source for the character facing down.
        size_hint (tuple): Size hint for the character.
        size (tuple): Size of the character.
        pos (tuple): Position of the character.
        direction (str): Current direction of the character.

    Methods:
        __init__: Initializes the Character instance.
        update_image: Updates the character's image based on the current direction.
        flip: Flips the character's direction.

    """
    def __init__(self, **kwargs):
        """
        Initialize the Character.

        Parameters:
            **kwargs: Additional keyword arguments for the Image.

        """
        super(Character, self).__init__(**kwargs)
        self.source_left = 'perso.png'
        self.source_right = 'perso_flip.png'
        self.source_up = 'perso_haut.png'
        self.source_down = 'perso_bas.png'
        self.size_hint = (None, None)
        self.size = (100, 100)
        self.pos = (100, 100)
        self.direction = 'left'
        self.update_image()

    def update_image(self):
        """
        Update the character's image based on the current direction.

        """
        if self.direction == 'left':
            self.source = self.source_left
        elif self.direction == 'right':
            self.source = self.source_right
        elif self.direction == 'up':
            self.source = self.source_up
        elif self.direction == 'down':
            self.source = self.source_down

    def flip(self):
        """
        Flip the character's direction.

        """
        if self.direction == 'left':
            self.direction = 'right'
        elif self.direction == 'right':
            self.direction = 'left'
        elif self.direction == 'up':
            self.direction = 'down'
        elif self.direction == 'down':
            self.direction = 'up'

        # Schedule the update_image method to be called after a short delay
        Clock.schedule_once(lambda dt: self.update_image())
