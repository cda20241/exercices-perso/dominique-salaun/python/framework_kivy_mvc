from kivy.app import App
from connexionMenu import ConnexionMenu

class MyApp(App):
    def build(self):
        return ConnexionMenu()

if __name__ == '__main__':
    MyApp().run()
